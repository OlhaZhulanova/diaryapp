import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddToDoBtnComponent } from './add-to-do-btn.component';

describe('AddToDoBtnComponent', () => {
  let component: AddToDoBtnComponent;
  let fixture: ComponentFixture<AddToDoBtnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddToDoBtnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddToDoBtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
