import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { NoteComponent } from './notes/note/note.component';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { ReactiveFormsModule } from '@angular/forms';
import {NavBarComponent} from './body/child-components/nav-bar/nav-bar.component';

@NgModule({
    declarations: [
        NoteComponent,
        NavBarComponent,
    ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    FormsModule,
    MatInputModule,
    MatCardModule,
    ReactiveFormsModule
  ],
    exports: [
        CommonModule,
        MatFormFieldModule,
        FormsModule,
        MatInputModule,
        NoteComponent,
        ReactiveFormsModule,
        NavBarComponent
    ],
  providers: []
}) export class SharedModule { }

