import { Pipe, PipeTransform } from '@angular/core';
import { Note } from '../notes/note';

@Pipe({ name: 'paginate' })
export class PaginatePipe implements PipeTransform {
  transform(items: Note[], page: number, itemsPerPage: number) {
    if (!items) {
      return [];
    }
    page++;
    return items.slice((page - 1) * itemsPerPage, page * itemsPerPage);
  }
}
