import { Pipe, PipeTransform } from '@angular/core';
import {Note} from '../notes/note';

@Pipe({ name: 'appSearch' })
export class FilterPipe implements PipeTransform {
  transform(items: Note[], searchText: string) {
    if (!items) {
      return [];
    }
    if (!searchText) {
      return items;
    }

    let notesArr: Note[];
    searchText = searchText.toLocaleLowerCase();

    return items.filter(it => {
      return it.title.toLocaleLowerCase().includes(searchText) ||
        it.content.toLocaleLowerCase().includes(searchText);
    });
  }}
