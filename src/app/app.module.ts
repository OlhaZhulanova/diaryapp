import { NgModule } from '@angular/core';

import { SharedModule } from './shared.module';
import { BrowserModule } from '@angular/platform-browser';
import { NotesModule } from './notes/notes.module';
import { RecycleBinModule } from './recycle-bin/recycle-bin.module';
import { Location } from '@angular/common';
import { environment } from 'src/environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { BodyComponent } from './body/body.component';
import { FooterComponent } from './footer/footer.component';
import { AuthPageComponent } from './core/auth/auth-page/auth-page.component';
import { MenuSideBarComponent } from './body/child-components/menu-side-bar/menu-side-bar.component';
import { AboutComponent } from './about/about.component';
import { EventSchedulerModule } from './scheduler/scheduler.module';
import { NgxLocalStorageModule } from 'ngx-localstorage';
import { CoreModule } from './core/core.module';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BodyComponent,
    FooterComponent,
    MenuSideBarComponent,
    AboutComponent,
    AuthPageComponent
  ],
  imports: [
    SharedModule,
    BrowserModule,
    AppRoutingModule,
    NotesModule,
    RecycleBinModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    EventSchedulerModule,
    NgxLocalStorageModule.forRoot(),
    CoreModule
  ],
  providers: [ Location ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
