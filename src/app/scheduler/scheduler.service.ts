import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {
  AngularFirestore
} from '@angular/fire/firestore';
import {AuthService} from '../core/auth/auth.service';
import {ScheduleEvent} from './schedule-event';
import {DataStorageService} from '../data-storage/data-storage.service';

export class EmptyEvent implements ScheduleEvent {
  color: undefined;
  id: '';
  ownerID: '';
  start: undefined;
  end: undefined;
  title: '';
}

@Injectable({
    providedIn: 'root'
  }
)
export class SchedulerService {
  eventsCollection: Observable<ScheduleEvent[]>;
  currentEvent: Subject<ScheduleEvent>;
  event: ScheduleEvent;

  editing: boolean;

  constructor(private db: AngularFirestore, private ds: DataStorageService, private auth: AuthService) {
    this.eventsCollection = this.ds.getItems('events') as Observable<ScheduleEvent[]>;
    this.currentEvent = new Subject<ScheduleEvent>();
    console.warn('events');
  }

  createEvent(formData) {
    if(this.validate(formData)) {
      this.ds.createItem('events', formData);
      alert('New event was saved to your calendar');
    }
    else
      alert('Event should have title and time start');
  }

  validate(event: ScheduleEvent) {
    console.log(event);
    if(!event)
      return false;
    if(event.title == '' || event.start == null || event.end == null)
      return false;
    else
      return true;
  }

  save() {
      if(this.validate(this.event)) {
        console.log(this.event);
        this.ds.updateItem('events', this.event.id, this.event);
      }
      else
        alert('Event should have title and time start');
  }

  delete(id: string) {
    this.ds.deleteItemPermanently('events', id);
    alert('Event was deleted');
  }

  getEvents() {
    return this.eventsCollection;
  }

  showEventPopup(event: any) {
    if(event != null) {
      this.editing = true;
      this.currentEvent.next(event);
      this.event = event;
    }
    else {
      this.editing = false;
      this.currentEvent.next(new EmptyEvent());
      this.event = new EmptyEvent();
    }
  }
}
