import {CalendarSchedulerEvent} from 'angular-calendar-scheduler';

export interface ScheduleEvent extends CalendarSchedulerEvent {
  ownerID: string;
}
