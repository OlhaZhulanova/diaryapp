import { NgModule } from '@angular/core';

import { SharedModule } from '../shared.module';
import { EventSchedulerComponent } from './event-scheduler.component';
import { CalendarCommonModule } from 'angular-calendar';
import { SchedulerModule } from 'angular-calendar-scheduler';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { registerLocaleData } from '@angular/common';
import { adapterFactory } from 'angular-calendar/date-adapters/moment';
import * as moment from 'moment';
import { EventPopupComponent } from './event/event-popup/event-popup.component';
import localeIt from '@angular/common/locales/it';
registerLocaleData(localeIt);

export function momentAdapterFactory() {
  return adapterFactory(moment);
}

@NgModule({
  declarations: [
    EventSchedulerComponent,
    EventPopupComponent
  ],
  imports: [
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: momentAdapterFactory,
    }),
    SchedulerModule.forRoot({ locale: 'en', headerDateFormat: 'daysRange', logEnabled: false }),
    CalendarCommonModule,
    SharedModule
  ]
})
export class EventSchedulerModule { }
