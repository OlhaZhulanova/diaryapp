import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {EmptyEvent, SchedulerService} from '../../scheduler.service';

import {AuthService} from '../../../core/auth/auth.service';
import {formatDate} from '@angular/common';

@Component({
  selector: 'app-event-popup',
  templateUrl: './event-popup.component.html',
  styleUrls: ['./event-popup.component.css']
})
export class EventPopupComponent implements OnInit {
  currentEvent;

  title: string;
  content: string;

  constructor(private sch: SchedulerService, private auth: AuthService) {
    this.sch.currentEvent.subscribe((ev)  => {
      if (ev.start && ev.end) {
        this.currentEvent = {
          ...ev,
          start: formatDate(new Date(ev.start), 'yyyy-MM-ddThh:mm', "en-US"),
          end: formatDate(new Date(ev.end), 'yyyy-MM-ddThh:mm', "en-US")
        };
      }
      else {
        this.currentEvent = new EmptyEvent();
      }
    });
  }

  @Output() closePopup: EventEmitter<any> = new EventEmitter<any>();
  @Output() deleteEvent: EventEmitter<any> = new EventEmitter<any>();

  ngOnInit(): void {
    console.log('init');
    this.currentEvent = new EmptyEvent();
  }

  parentClosePopup() {
    this.closePopup.emit();
  }

  parentDeleteEvent() {
    this.deleteEvent.emit(this.currentEvent);
  }

  save() {
    if(!this.sch.editing) {
      const formData = {
        title: this.currentEvent.title ?? '',
        content: this.currentEvent.content ?? '',
        start: this.currentEvent.start ?? '',
        end: this.currentEvent.end ?? '',
        ownerID: this.auth.currentUserId
      }
      this.sch.createEvent(formData);
    }
    else {
      this.sch.save();
    }

  }
}
