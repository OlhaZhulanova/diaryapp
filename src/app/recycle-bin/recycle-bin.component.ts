import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Note } from '../notes/note';
import { NoteService } from '../notes/notes.service';
import { AuthService } from '../core/auth/auth.service';
import { DataStorageService } from '../data-storage/data-storage.service';
import { RecycleBinService } from './recycle-bin.service';

@Component({
  selector: 'app-recycle-bin',
  templateUrl: './recycle-bin.component.html',
  styleUrls: ['./recycle-bin.component.css']
})
export class RecycleBinComponent implements OnInit {
  notes: Observable<Note[]>;
  timeoutDays: number = 5;

  constructor(private noteService: NoteService,
              private auth: AuthService,
              private ds: DataStorageService,
              private rs: RecycleBinService) {
    this.notes = this.noteService.getDeletedNotes();
  this.timeoutDays = this.rs.timeoutDays;
  }

  ngOnInit(): void {
    this.rs.deleteExpiredNotes(this.timeoutDays);
    console.log('delete');
  }

  changeTimeout(event) {
    console.log(event.target.value);
    if (!this.rs.changeTimeout(event.target.value)) {
      this.timeoutDays = 365;
      this.rs.changeTimeout(this.timeoutDays)
    };
  }
}
