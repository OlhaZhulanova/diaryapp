import { NgModule } from '@angular/core';

import { SharedModule } from '../shared.module';
import { RecycleBinComponent } from './recycle-bin.component';
import { NotesModule } from '../notes/notes.module';

@NgModule({
  imports: [
    SharedModule,
    NotesModule
  ],
    declarations: [
      RecycleBinComponent
    ],
    exports: [
    ]
})
export class RecycleBinModule { }
