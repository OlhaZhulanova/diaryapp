import { Injectable } from '@angular/core';

import { DataStorageService } from '../data-storage/data-storage.service';
import { Note } from '../notes/note';
import { LocalStorageService } from 'ngx-localstorage';

@Injectable({
  providedIn: 'root'
})
export class RecycleBinService {
  constructor(private ds: DataStorageService,
              private lss: LocalStorageService) {
    const timeout = this.lss.get('timeoutDays');
    if(timeout)
      this.timeoutDays = timeout;
  }

  timeoutDays: number = 5;

  changeTimeout(value): boolean {
    if (value > 365) {
      alert('Please, enter a number of days less than 365');
      return false;
    }
    else {
      this.lss.set('timeoutDays', value);
      return true;
    }
  }

  deleteExpiredNotes(days: number) {
    const currentDate = new Date();

    this.ds.getItems('notes').subscribe(
      notes => {
        notes.map((note: Note) => {
          if(note.isDeleted)
            if(currentDate.getDay() - note.deleted.toDate().getDay() >= this.timeoutDays)
              this.ds.deleteItemPermanently('notes', note.id);
        })
      }
    )
  }
}
