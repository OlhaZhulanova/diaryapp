import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-auth-page',
  templateUrl: './auth-page.component.html',
  styleUrls: ['./auth-page.component.css']
})
export class AuthPageComponent implements OnInit {

  constructor(private auth: AuthService) { }

  ngOnInit(): void {
  }

  logout() {
    this.auth.logout();
  }

  loginViaGoogle(): void {
    if(this.auth.authenticated) {
      alert('You are authorized yet');
      return;
    }
    else
      this.auth.loginViaGoogle();
}

}
