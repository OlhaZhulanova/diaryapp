import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import OAuthCredential = firebase.auth.OAuthCredential;
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
  }
)
export class AuthService {
  authState: any = null;
  provider = new firebase.auth.GoogleAuthProvider();
  authWatcher: Observable<any>;
  nextRoute: string = null;

  constructor(public auth: AngularFireAuth, private router: Router) {
    console.log('auth');
    this.auth.authState.subscribe( data => { this.authState = data; });
  }

  public subscribe(onNext: (value: any) => void, onThrow?: (exception: any) => void, onReturn?: () => void) {
    return this.auth.authState.subscribe(onNext, onThrow, onReturn);
  }

  get authenticated(): Observable<any> {
    return this.authState;
  }

  get currentUserId(): string {
    return this.authenticated ? this.authState.uid : null;
  }

  get currentUserName(): string {
    return this.authenticated ? this.authState.displayName : null;
  }

  loginViaGoogle() {
    this.auth.signInWithPopup(this.provider)
      .then((result) => {
        const token = (result.credential as OAuthCredential).accessToken;
        const user = result.user;
        console.log('success');
        this.router.navigateByUrl(this.nextRoute);
        console.log(this.nextRoute);
      }).catch((error) => {
      const errorCode = error.code;
      const errorMessage = error.message;
      const email = error.email;
      const credential = error.credential;
      console.log(errorMessage);
      alert(`${errorCode} : ${errorMessage}`);
    });
  }

  logout(): void {
    console.log('called Logout');
    this.auth.signOut();
  }


}
