import { NgModule } from '@angular/core';
import { AuthService } from './auth/auth.service';
import { DataStorageService } from '../data-storage/data-storage.service';
import { NoteService } from '../notes/notes.service';
import { SchedulerService } from '../scheduler/scheduler.service';
import { RecycleBinService } from '../recycle-bin/recycle-bin.service';
import { DiaryGuard } from '../guards/diary.guard';
import { LocalStorageService } from 'ngx-localstorage';

@NgModule({
  imports: [],
  providers: [
    AuthService,
    DataStorageService,
    NoteService,
    SchedulerService,
    RecycleBinService,
    DiaryGuard,
    LocalStorageService
  ]
})
export class CoreModule { }
