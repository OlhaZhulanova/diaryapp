import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { ElementRef, ViewChild } from '@angular/core';
import { LocalStorageService } from 'ngx-localstorage';

export enum background {
  sky = 'sky',
  play = 'play',
  flowers = 'flowers',
  desk = 'desk'
}

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  date: Observable<Date>;
  currentDate: Date;
  currentBackground = background.sky;
  titleColor: any = '#f0f5f5';
  mainColor: any = '#f0f5f5';

  background = background;

  @Output() changeBack: EventEmitter<background> = new EventEmitter<background>();
  @Output() changeMainColor: EventEmitter<any> = new EventEmitter<any>();


  @ViewChild('themesDropDown') dropdown: ElementRef<HTMLDivElement>; // Angular Version 8
  isShown = false;
  constructor(private lss: LocalStorageService) {
    this.currentDate = new Date();

    const userTitleColor = this.lss.get('titleColor');
    if(userTitleColor)
      this.titleColor = userTitleColor;

    const userMainColor = this.lss.get('mainColor');
    if(userTitleColor)
      this.mainColor = userMainColor;
  }

  ngOnInit(): void {
  }

  parentChangeBack(color: background){
    console.log(color);
    this.changeBack.emit(color);
  }

  changeTitle() {
    console.log(this.titleColor);
    this.lss.set('titleColor', this.titleColor);
  }

  parentChangeMainColor() {
    this.lss.set('mainColor', this.mainColor);
  }

  manageThemePicker() {
    this.isShown = !this.isShown;
  }

  getBacks() {
    return Object.keys(background)
      //.filter(StringIsNumber)
      .map(key => background[key]);
  }
}
