import { Injectable}  from '@angular/core';

import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Note } from '../notes/note';
import { ScheduleEvent } from '../scheduler/schedule-event';
import firebase from 'firebase';
import { DataStorage } from './data-storage.interface';
import Timestamp = firebase.firestore.Timestamp;
import Reference = firebase.database.Reference;
import { AuthService } from '../core/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class DataStorageService implements DataStorage {
  dbRef: Reference;

  constructor(private db: AngularFirestore,
              private auth: AuthService) {
    this.dbRef = firebase.database().ref();
  }

  getItemById(collectionName: string, id: string) {
    console.log('id', id);
    return firebase.firestore().collection(collectionName)
      .where('id', '==', id).get();
  }

  getItems(collectionName: string) {
    return this.db.collection(collectionName).valueChanges().pipe(
      map(items => {
        return items.filter((item: Note | ScheduleEvent) => {
          console.log(item.ownerID, this.auth.currentUserId);
          return item.ownerID == this.auth.currentUserId;
        })
        }
      )
    );
  }

  updateItem(collectionName: string, id: string, formData: any) {
    this.getItemById(collectionName, id)
      .then(snap => {
        snap.forEach(doc => {
          doc.ref.update(formData);
          console.log('doc: ', doc);
        });
      });
  }

  deleteItemPermanently(collectionName: string, id: string) {
    this.getItemById(collectionName, id)
      .then(snap => {
        snap.forEach(doc => {
          doc.ref.delete();
        });
      });
  }

  deleteItem(collectionName: string, id: string) {
    this.getItemById(collectionName, id)
      .then(snap => {
        snap.forEach(doc => {
          doc.ref.update({isDeleted: true, deleted: Timestamp.fromDate(new Date())});
        });
      });
  }

  restoreItem(collectionName: string, id: string) {
    this.getItemById(collectionName, id)
      .then(snap => {
        snap.forEach(doc => {
          doc.ref.update({isDeleted: false, deleted: null});
        });
      });
  }

  createItem(collectionName: string, formData: any) {
    let id = this.db.createId();
    console.log({val: formData});
    this.db.collection(collectionName).add({id, ...formData});

  }
}
