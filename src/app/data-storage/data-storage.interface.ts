export interface DataStorage {
  getItems(collectionName: string);
  updateItem(collectionName: string, id: string, formData: any);
  createItem(collectionName: string, formData: any);
  deleteItem(collectionName: string, id: string);
  deleteItemPermanently(collectionName: string, id: string)
}
