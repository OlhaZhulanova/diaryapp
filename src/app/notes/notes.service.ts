import { Injectable } from '@angular/core';

import { Note } from './note';
import { map } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable, Subject } from 'rxjs';
import firebase from 'firebase';
import { AuthService } from '../core/auth/auth.service';
import { DataStorageService } from '../data-storage/data-storage.service';
import { Router } from '@angular/router';
import { sortOrderEnum, sortValueEnum } from './notes-list/notes-list.component';
import Timestamp = firebase.firestore.Timestamp;

export class EmptyNote implements Note {
  title: '';
  content: '';
  created: null;
  ownerName: '';
  ownerID: '';
  edited: null;
  isDeleted: false;
  deleted: null;
}

@Injectable({
    providedIn: 'root'
  }
)
export class NoteService {
  notesCollection: Observable<Note[]>;
  noteDoc: AngularFirestoreDocument<Note>;
  editing: boolean;
  creating: boolean;
  title: string;
  currentNote: Subject<Note>;
  note: Note;

  constructor(private db: AngularFirestore,
              private auth: AuthService,
              private ds: DataStorageService,
              private router: Router) {
    this.notesCollection = this.ds.getItems('notes') as Observable<Note[]>;
    this.note = new EmptyNote();
  }

  getNotes(sortValue: string = 'created', sortOrder: number = sortOrderEnum.asc): Observable<Note[]> {
    return this.notesCollection.pipe(
      map(notes => {
        if(sortValue == sortValueEnum.title) {
          return notes.filter(note => !note.isDeleted).sort((a, b) => {
            if(sortOrder == sortOrderEnum.asc)
              return a.title.localeCompare(b.title);
            else
              return b.title.localeCompare(a.title);
            })
        }
        else {
            return notes.filter(note => !note.isDeleted).sort((a, b) => {
              if(sortOrder == sortOrderEnum.asc)
                return a[sortValue] - b[sortValue];
              else
                return b[sortValue] - a[sortValue];
            });
        }
      }
  )).pipe(map(notes => notes.filter(note => !note.isDeleted)))
  }

  getDeletedNotes(): any {
    return this.notesCollection.pipe(
      map(notes => notes.filter(note => note.isDeleted))
    );
  }

  getNoteData(id: string): Observable<Note> {
    this.noteDoc = this.db.doc<Note>(`posts/${id}`);
    return this.noteDoc.valueChanges();
  }

  private validate(data: Note) {
    if (data.title == '') {
      alert('Note should have at least a title');
      return false;
    }
    return true;
  }

  create(data: Note): boolean {
    if (!this.validate(data)) {
      return false;
    }

    this.ds.createItem('notes', {
      ownerID: this.auth.currentUserId,
      ownerName: this.auth.currentUserName,
      created: Timestamp.fromDate(new Date()),
      title: data.title,
      content: data.content,
      isDeleted: false,
      deleted: null
    });
    alert('New note was saved. You can edit it at any time');
    return true;
  }

  delete(id: string): void {
    let query = firebase.firestore().collection('notes')
      .where('id', '==', id).get()
      .then(snap => {
        snap.forEach(doc => {
          doc.ref.update({isDeleted: true, deleted: Timestamp.fromDate(new Date())});
        });
      });
    this.ds.getItemById('notes', id);
  }

  update(formData): boolean {
    if (!this.validate(this.note)) {
      return false;
    }
    this.ds.updateItem('notes', this.note.id, this.note);
    alert('Modified note was saved');
    return true;
  }

  showEditor(note: Note) {
    if(note) {
      this.editing = true;
      this.note = note;
    }
    else {
      this.editing = false;
      this.note = new EmptyNote();
    }
    this.router.navigateByUrl('editor');
  }

  addTag() {

  }
}
