import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { NoteService } from '../notes.service';
import { Note } from '../note';
import { Observable } from 'rxjs';
import { AuthService } from '../../core/auth/auth.service';
import { NoteEditorComponent } from '../note-editor/note-editor.component';
import { NoteComponent } from '../note/note.component';

export enum sortValueEnum {
  created = 'created',
  edited = 'edited',
  title = 'title'
}

export enum sortOrderEnum {
  asc,
  desc
}

@Component({
  selector: 'app-notes-list',
  templateUrl: './notes-list.component.html',
  styleUrls: ['./notes-list.component.css']
})
export class NoteListComponent implements OnInit {
  notes: Observable<Note[]>;
  notesCount: number;
  editor: NoteEditorComponent;

  title: string;
  content: string;
  searchText: string;
  currentSortValue: string;
  currentSortOrder: number;

  pages: number[];
  itemsPerPage: number = 3;
  currentPage: number = 0;

  sortValue = sortValueEnum;
  sortOrder = sortOrderEnum;

   @ViewChild('app-note') noteChild: NoteComponent;

  constructor(private noteService: NoteService,
              private auth: AuthService) {
    this.notes = this.noteService.getNotes(sortValueEnum.created);
    this.notes.subscribe((notes) => {
      let pagesCount = Math.ceil(notes.length / this.itemsPerPage);
      this.pages = [];
      for(let page = 0; page < pagesCount; page++) {
        this.pages[page] = page;
      }
      console.log(pagesCount);
    });
  }

  ngOnInit(): void {
  }

  sort() {
    console.log(this.currentSortOrder);
    this.notes = this.noteService.getNotes(this.currentSortValue, this.currentSortOrder);
  }

  delete(id: string): void {
    this.noteService.delete(id);
    alert('Note was moved to recycle bin');
  }

  update(note: Note): void {
    this.noteService.note = note;
    this.noteService.editing = true;
  }

  sendData(note: Note): void {
    this.noteService.showEditor(note);
  }

  setCurrentPage(page) {
    this.currentPage = page;
  }
}
