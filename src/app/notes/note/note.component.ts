import { Component, EventEmitter, Input, Output , OnInit} from '@angular/core';
import { Note } from '../note';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.css']
})
export class NoteComponent implements OnInit {

  @Input() note: Note;
  @Input() fromDashboard: boolean;

  @Output() edit: EventEmitter<Note> = new EventEmitter<Note>();
  @Output() delete: EventEmitter<string> = new EventEmitter<string>();
  @Output() restore: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  parentEdit(note: Note) {
    this.edit.emit(note);
  }

  parentDelete(id: string) {
    this.delete.emit(id);
  }

  parentRestore(id: string) {
    this.restore.emit(id);
  }

}
