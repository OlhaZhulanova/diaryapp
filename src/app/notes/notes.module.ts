import { NgModule } from '@angular/core';

import { SharedModule } from '../shared.module';
import { MatCardModule } from '@angular/material/card';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { NoteEditorComponent } from './note-editor/note-editor.component';
import { NoteListComponent } from './notes-list/notes-list.component';
import { FilterPipe } from '../pipes/filter.pipe';
import { PaginatePipe } from '../pipes/paginate.pipe';
import { AppRoutingModule } from '../app-routing.module';

@NgModule({
  imports: [
    SharedModule,
    MatCardModule,
    MatButtonModule,
    BrowserAnimationsModule,
    MatIconModule,
    AppRoutingModule
  ],
    declarations: [
        NoteEditorComponent,
        NoteListComponent,
        FilterPipe,
        PaginatePipe
    ],
  exports: [
  ],
  providers: [
  ]
})
export class NotesModule { }
