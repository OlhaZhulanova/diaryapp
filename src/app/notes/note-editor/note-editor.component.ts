import { Component, OnInit } from '@angular/core';
import { NoteService } from '../notes.service';
import { Observable } from 'rxjs';
import { Note } from '../note';
import { AuthService } from '../../core/auth/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import firebase from 'firebase';
import Timestamp = firebase.firestore.Timestamp;

@Component({
  selector: 'app-note-editor',
  templateUrl: './note-editor.component.html',
  styleUrls: ['./note-editor.component.css']
})
export class NoteEditorComponent implements OnInit {
  notes: Observable<Note[]>;
  myNotes: any;
  editing: boolean;
  creating: boolean;
  title: string;
  content: string;
  currentNote: Note;
  addingSucceeded: boolean;
  addingTag: boolean = false;

  constructor(private noteService: NoteService,
              private auth: AuthService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.currentNote = this.noteService.note;
  }

  navigate() {
    if (this.addingSucceeded)
      this.router.navigateByUrl('/notes');
    else
      this.router.navigateByUrl('/editor');
  }

  saveNote() {
    if(!this.noteService.editing) {
      this.addingSucceeded = this.noteService.create(this.currentNote);
      this.editing = false && this.addingSucceeded;
    }
    else {
      this.addingSucceeded = this.noteService.update(this.currentNote);
    }
    this.navigate();
  }

  addTag() {
    this.editing = true;
    this.addingTag = !this.addingTag;
  }

  saveTag() {
    this.saveNote();
  }

  deleteTag() {
    this.addingTag = false;
  }
}
