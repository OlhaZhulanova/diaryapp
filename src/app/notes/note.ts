import firebase from 'firebase';
import Timestamp = firebase.firestore.Timestamp;

export class Note {
    id?: string;
    title: string;
    content?: string;
    ownerName: string;
    ownerID: string;
    created: Timestamp;
    edited?: Timestamp;
    isDeleted: boolean;
    deleted?: Timestamp;
    tag?: string;
}
