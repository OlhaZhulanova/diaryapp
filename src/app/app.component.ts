import { Component, ViewChild } from '@angular/core';
import { AuthService } from './core/auth/auth.service';
import { background } from './header/header.component';
import { LocalStorageService } from 'ngx-localstorage';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'DiaryApp';
  back: background = background.sky;
  titleColor: any;
  mainColor: any;

  @ViewChild('app-header') header;

  constructor(private auth: AuthService,
              private lss: LocalStorageService) {
    const userColor = this.lss.get('back');
    if(userColor)
      this.back = userColor;

    const userMainColor = this.lss.get('mainColor');
    if(userMainColor)
      this.mainColor = userMainColor;
  }

  changeBack(color: background) {
    this.back = color;
    this.lss.set('back', color);
  }


  changeMainColor(color: any) {
    this.mainColor = color;
    this.lss.set('mainColor', color);
  }
}
