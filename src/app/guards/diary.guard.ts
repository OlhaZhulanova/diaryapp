import { Injectable } from '@angular/core';

import { AuthService } from '../core/auth/auth.service';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class DiaryGuard implements CanActivate {

  constructor(private authService: AuthService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (!this.authService.authenticated) {
      this.authService.nextRoute = state.url;
      console.log('state: ',state.url);
      this.authService.loginViaGoogle();
      return false;
    }
    return  true;
  }
}
