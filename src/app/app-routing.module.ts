import { NgModule } from '@angular/core';

import { SharedModule } from './shared.module';
import { RouterModule, Routes } from '@angular/router';
import { RecycleBinComponent } from './recycle-bin/recycle-bin.component';
import { NoteListComponent } from './notes/notes-list/notes-list.component';
import { NoteEditorComponent } from './notes/note-editor/note-editor.component';
import { AuthPageComponent } from './core/auth/auth-page/auth-page.component';
import { AboutComponent } from './about/about.component';
import { DiaryGuard } from './guards/diary.guard';
import { EventSchedulerComponent } from './scheduler/event-scheduler.component';

const routes: Routes = [
  {
    path: 'calendar',
    component: EventSchedulerComponent,
    canActivate: [DiaryGuard]
  },
  {
    path: 'editor',
    component: NoteEditorComponent,
    canActivate: [DiaryGuard]
  },
  {
  path: 'notes',
  component: NoteListComponent,
  canActivate: [DiaryGuard]
},
{
  path: 'recycle-bin',
  component: RecycleBinComponent,
  canActivate: [DiaryGuard]
},
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: 'authorization',
    component: AuthPageComponent
  },
{
  path: '',
  redirectTo: '/about',
  pathMatch: 'full'
},
{
  path: '**',
  redirectTo: '/about',
  pathMatch: 'full'
}];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    SharedModule
  ],
  exports: [ RouterModule ]

})
export class AppRoutingModule { }
