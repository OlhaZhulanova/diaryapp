const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

exports.addMessage = functions.https.onRequest(async (req, res) => {
  // Grab the text parameter.
  const original = req.query.text;
  // Push the new message into Firestore using the Firebase Admin SDK.
  const writeResult = await admin.firestore().collection('messages').add({original: original});
  // Send back a message that we've successfully written the message
  res.json({result: `Message with ID: ${writeResult.id} added.`});
});

exports.fcmSend = functions.database
  .ref('/messages/{userId}/{messageId}')
  .onCreate(event => {
    const message = event.data.val();
    const userId = event.params.userId;

    const payload = {
      notification: {
        title: message.title,
        body: message.body
      }
    }

    admin.database()
      .ref(`/fcmTokens/${userId}`)
      .once('value')
      .then(token => {
        return admin.messaging().sendToDevice(token, payload);
      })
      .then(res => {
        console.log('Sent successfully', res);
      })
      .catch(err => {
        console.log(err);
      })
  })
