import { environment } from "./environments/environment";

importScripts('https://www.gstatic.com/firebasejs/7.6.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.6.0/firebase-messaging.js');
firebase.initializeApp(environment.firebase);
const messaging = firebase.messaging();
